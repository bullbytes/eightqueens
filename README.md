# What is this?

The start of a solution to [Problem 750 - 8 Queens Chess Problem](https://www.udebug.com/UVa/750).

Reading in the input is not implemented.

The main class is in `src/main/java/Start.java`.

# Build instructions

    ./gradlew run

# Requirements

* JDK 8
