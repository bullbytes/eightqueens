import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.abs;

/**
 * <p>
 * Person of contact: Matthias Braun
 */
public class Start {

    public static void main(String... args) {
        // https://www.udebug.com/UVa/750

        // Queen in column a has to be in row b
        int a = 0;
        int b = 0;
        System.out.print("SOLN COLUMN\n");
        System.out.print(" # 1 2 3 4 5 6 7 8\n\n");

        // Generate all possible 8! candidate solutions
        List<int[]> solutionCandidates = new ArrayList<>();
        placeQueens(0, new int[8], solutionCandidates);

        printSolutionForQueenAt(a, b, solutionCandidates);
    }

    private static void printSolutionForQueenAt(int a, int b, List<int[]> solutionCandidates) {
        for (int solutionNumber = 0; solutionNumber < solutionCandidates.size(); solutionNumber++) {
            int[] rows = solutionCandidates.get(solutionNumber);
            // If there's a queen at the desired position (a,b) we have a solution
            if (rows[b] == a) {
                // Print the solution number
                System.out.printf("%2d", 1 + solutionNumber);

                // Print the solution: the rows where each queen is placed
                for (int j = 0; j < 8; j++) {
                    System.out.printf(" %d", rows[j] + 1);
                }
                System.out.println();
            }
        }
    }

    /**
     * Create all solutions for the eight queen problem.
     *
     * @param c         the column number of the current queen
     * @param rows      the row numbers of the queens where they can't take each other.
     *                  rows[c] gives us the row of the queen at column c
     * @param solutions contains rows where all eight queens can't take each other
     */
    private static void placeQueens(int c, int[] rows, List<int[]> solutions) {
        // We have placed all queens in a way that they can't take each other
        if (c == 8) {
            solutions.add(rows.clone());
        }
        // Try all possible rows
        for (int r = 0; r < 8; r++) {
            // Check if we can place a queen at the rows and column so she can't be taken by other queens
            if (canPlaceQueen(r, c, rows)) {
                // Place the queen of column c at rows r
                rows[c] = r;
                // Try to place the next queen
                placeQueens(c + 1, rows, solutions);
            }
        }
        // The interesting bit is after the for-loop: If we couldn't place a queen anywhere, we backtrack!
        // Meaning we jump back inside the for-loop of the previous stack frame. In there, we'll place the
        // queen in another row
    }


    private static boolean canPlaceQueen(int r, int c, int[] row) {
        // Check previously placed queens
        for (int prev = 0; prev < c; prev++)
            if (row[prev] == r || (abs(row[prev] - r) == abs(prev - c)))
                // Queens share row or diagonal -> can't place the queen
                return false;
        return true;
    }

    private static void howToParseInput() {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        while (testCases-- > 0) {

            String line = scanner.nextLine();
            Pattern pattern = Pattern.compile("(\\d+) (\\d+)");
            Matcher result = pattern.matcher(line);
            int a = Integer.parseInt(result.group(1));
            int b = Integer.parseInt(result.group(2));
            // switch to zero-based indexing
            a--;
            b--;
        }
    }
}
